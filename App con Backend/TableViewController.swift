//
//  TableViewController.swift
//  App con Backend
//
//  Created by Katherine Vela on 6/6/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import UIKit
import Alamofire

class TableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tb: UITableView!
    
    var usuarios:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let back = BackendManager()
        back.getData { (respuesta) in
            self.usuarios = respuesta
            self.tb.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        
        
        let elemento = usuarios[indexPath.row] as? NSDictionary
        let nickName = elemento?["nick_name"] as? String
        let userName = elemento?["user_name"] as? String
        cell.textLabel?.text = "\(nickName!)  \(userName!)"
        
        return cell
    }


    /*@IBOutlet weak var tb: UITableView!
    var usuarios:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let back = BackendManager()
        back.obtenerDatos()
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos(_:)), name: NSNotification.Name("actualizar"), object: nil)
    }
    
    func actualizarDatos (_ notification: Notification){
        usuarios = notification.userInfo?["response"] as! NSArray
        
        DispatchQueue.main.async {
            self.tb.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        
        
        let elemento = usuarios[indexPath.row] as? NSDictionary
        let nickName = elemento?["nick_name"] as? String
        let userName = elemento?["user_name"] as? String
        cell.textLabel?.text = "\(nickName!)  \(userName!)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "hola"
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }*/


}
