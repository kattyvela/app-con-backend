//
//  BackendManager.swift
//  App con Backend
//
//  Created by Katherine Vela on 23/5/17.
//  Copyright © 2017 Katherine Vela. All rights reserved.
//

import Foundation
import Alamofire

class BackendManager {
    
    func insertData (user:String, nick:String, password:String) {
        
        let urlString = "https://serivcio-web-json.mybluemix.net/insert_data?user_name=\(user)&nick_name=\(nick)&password=\(password)"
        
        Alamofire.request(urlString).responseJSON {
            response in debugPrint(response)
            
        }
        
    }
    
    /*func obtenerDatos (){
        
        let urlString = "https://serivcio-web-json.mybluemix.net/get"
        
       
        
        Alamofire.request(urlString).responseJSON {
            response in //debugPrint(response)
            
            guard let json = response.result.value! as? NSArray else {
                print("error")
                return
            }
            
            NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object:nil, userInfo: ["response": json ])
            
        }
        
        
    }
    
    func getData(completionHandler: @escaping (NSArray)-> Void) {
        
        let urlString = "https://serivcio-web-json.mybluemix.net/get"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            guard let JSON = response.result.value as? NSArray else{
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(JSON)
            }
            
        }
        
    }*/
    
    func getData(completionHandler: @escaping (NSArray)-> Void) {
        
        let urlString = "https://serivcio-web-json.mybluemix.net/get"
        
        Alamofire.request(urlString).responseJSON {
            response in
            
            guard let JSON = response.result.value as? NSArray else{
                return
            }
            
            DispatchQueue.main.async {
                completionHandler(JSON)
            }
            
        }
        
    }


}
